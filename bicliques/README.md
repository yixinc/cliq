#  Finding Bicliques in Graphs

-------------------

### Purpose of this area.

This repo contains codes to compute bicliques in graphs.

New codes should be added as they are found and demonstrated to be useful.

No code should be removed or deleted.

-------------------

## Directory bicliques 

Contains codes for identifying bicliques in graphs.

-------------------

### Subdirectories:   src01 and test01

I have been using this codes since 2014.  I chose it out of durress, to meet
a deadline, and have been using it ever since.
I do not know whether there is some better code out there now.


**src01:**
This is the source code.  
A few notes:

1. The code has been updated just today to use python 3.
2. Also, the code has been modified so that the nodes in a biclique are
output in increasing order.
3. The ordering, though, is based on text values, not integer (node ID) values.
This means, for example, that the number 100 will appear before number 3
because 1 < 3.
This is ok; we just want a repeatable order.
4. This is a serial code.



**test01:**
This is the test directory.
There are three test cases (that is, three different executions of the code).

To run tests:

1. go to the test01 directory.
2. create a conda python 3 virtual environment to run code in (this needs to be done only once).
    - see instructions below.
3. activate the conda virtual environment.
    - in example below, type:  conda activate py37-base
4. to run the test cases, type:  ./run.all
5. to test the output against valid output, type:  ./run.diff.all

Step 5 is a simple unix diff command, so no output means no differences between the
just-run output and the valid output.

Note there may be differences in the order in which the bicliques appear in the output file.
I suppose this could be changed by altering the code to accept a seed for the random
number generator.

-------------------

### command line invocation

python ../src01/findAllMaximalBicliques.py  graph.01.inp   1-means-graph-is-a-uel-file   min-num-nodes-in-each-bipartition  output-file-for-bicliques

Example:
python ../src01/findAllMaximalBicliques.py  graph.01.inp     1    2  graph.01.bicliques.out


### Input file format
input-graph-file:  the graph is undirected.
Each line contains one edge of the graph.
There are two nodes on a line that form one edge.
So one graph edge per line.
See the test cases for examples.


### Output file format
output-filename-for-cliques:  
In the single output file, the nodes of one clique are printed on one line of the
file.
So if there are q cliques in a graph, then there should be q lines in the output file.
The node ordering on a line is not important, so two lines can be the same, with the
same node IDs, but in different orders; these are the same bicliques.

-------------------

## Setting up environment to run python code.

### On laptop, using conda.

Create a conda virtual environment.  Any python version >= python 3.7 should be fine.
I have tested code with python 3.7.

1.  At command line, outside of any virtual environment, type:  conda create -n myenv python=3.7 
    - where myenv is the name of the virtual environment (venv) that you specify.
    - assume that we use py37-base for the venv.
    - assume that we use python 3.7
    - so, this is:  conda create -n py37-base python=3.7

To activate the conda venv, do:

1. get out of any conda venv:  conda deactivate
2. activate this venv:  conda activate py37-base


