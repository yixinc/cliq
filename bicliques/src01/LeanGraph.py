# -*- coding:utf-8 -*-

### ckuhlman. 
### 19 Sep 2013.

### Purpose:  This is a lean graph; i.e., it is a graph
###           with no labels.
###           Graphs are stored as adj lists.
###           An undirected graph has two edges stored internally:
###           edge  a--b is a-->b and b-->a.

###       An edge (a,b) means a-->b.
###       Node a is a tail node; node b is the head node.
###       An isolated node has not edges incident on it.
###       An isolated node is neither a tail nor a head node.


### ------------------------------------------
### Imports.
### For file reading, writing.
import os.path
### For deep copying.
import copy


### ------------------------------------------
### Attributes:
###    isdirected:  means that 
###         when reading data from file, will
###         read (a,b), and if the graph is 
###         undirected, will store data
###         in this object as (a,b) and (b,a).
###    If the graph is directed, then just (a,b); 
###    i.e., a-->b.


### ------------------------------------------
### Reading graph from, and writing graph to, file.
### If directed graph, can write:
###     -adj list.
###     -DEL (directed edge list), a b   for a-->b.
### If undirected graph, can write:
###     -adj list.
###     -adj list with only 1/2 of edges a  b for a<-->b where a<b.
###     -UEL (undirected edge list), a b   for a-->b.


### ------------------------------------------
### In the adjacency list storage structure,
### for edge tail-->head, head is stored in a set.


class LeanGraph:
    """ Double keyed map (dictionary)
    """

    # ------------------------------------------
    ## Steve, this is where "Verified." will go.
    ## Verified.
    def __init__(self,isdirected=1):
         """ Constructor.  Arg 1 means directed keys.
             If isdirected=1, then the graph is directed.
             If isdirected=0, then the graph is undirected.
         """
         # Initialize the dict.
         self.entries = {}
         # Default is directed graph.
         self.isdirected=isdirected

         # Current set of isolated nodes.
         self.isolated = set()

    # ------------------------------------------
    ## Verified.
    def hasTail(self,key01):
        """Does tail node (key01) exist; return True if so.
           Return:  True if key01 exists; False otherwise.
        """
        ## return self.entries.has_key(key01)
        if key01 in self.entries:
            return True
        else:
            return False
    
    # ------------------------------------------
    ## Verified.
    def hasHead(self,key01):
        """Does head node (key01) exist; return True if so.
           Return:  True if key01 exists; False otherwise.
        """
        if self.isdirected:
            for item in self.entries.keys():
                heads = self.entries[item]
                if key01 in heads:
                    return True
            ## Nothing found.
            return False
        else:
            return self.hasTail(key01)
    

    # ------------------------------------------
    ## Verified.
    def isDirected(self):
        """If keys are directed, return 1.
           Return:  0 if graph is not directed.
        """
        return self.isdirected
    
    # ------------------------------------------
    # Verified.
    def hasNode(self,key01):
        """If a node (key01) appears in graph, return True; false otherwise.
        """
        if self.hasTail(key01):
            return True
        elif self.hasHead(key01):
            return True
        elif key01 in self.isolated:
            return True

        return False
    
    # ------------------------------------------
    ## Verified.
    def hasEdge(self,key01,key02):
        """Does the edge key01-->key02 exist?
           Does (key01,key02) exist; i.e., edge key01-->key02.
           Return True if so.
           Return:  True if (key01,key02) exists; False otherwise.
        """
        if (self.hasTail(key01)):
            inner=self.entries[key01]
            if key02 in inner:
                return True
            else:
                return False
        else:
            return False

    # ------------------------------------------
    # Validated.
    def deleteSingleDirectedEdge(self,key01,key02):
        """ Delete edge key01-->key02.
            This is only a single, even if graph is directed.
            Return True if edge is deleted;
            False otherwise.
            Use with caution; this is mostly a helper function.
        """
        if (self.hasTail(key01)):
            heads = self.entries[key01]
            if key02 in heads:
                heads.remove(key02)
                if not heads:
                    ## Empty.
                    del self.entries[key01] 
                return True
            else:
                return False
        else:
            return False

    # ------------------------------------------
    # Validated.
    def deleteEdgesIncidentOnTail(self,key01):
        """ Delete a tail node and all incident edges.
            For directed graphs, these edges are only those
            edges of the form:  key01 --> headNode.
            For undirected graphs, it is all incident edges:
            incoming and outgoing.
            Return True if key01 and entries deleted;
            False otherwise.
        """
        if (self.isdirected):
            ## Delete only those edges that have key01 as tail.
            if key01 in self.entries.keys():
                ## Delete this tail and all heads.
                heads = self.entries[key01]
                heads.clear()
                del self.entries[key01]
                return True
            else:
                return False
        else:
            ## Graph undirected.  So delete 
            ## all incident edges.
            if key01 in self.entries:
                heads = self.entries[key01]
                for item in heads:
                    ## Delete edges (head, tail).
                    self.deleteSingleDirectedEdge(item,key01)
                heads.clear()
                del self.entries[key01]
                return True
            else:
                return False
        # Does not exist, return False;
        return False


    # ------------------------------------------
    # Validated.
    def deleteEdgesIncidentOnHead(self,key01):
        """ Delete a head node (key01) and all incident edges.
            For directed graphs, these edges are only those
            edges of the form:  tailNode --> key01.
            For undirected graphs, it is all incident edges:
            incoming and outgoing; i.e., tailNode <--> key01.
            Return True if key01 and entries deleted;
            False otherwise.
        """
        if (self.isdirected):
            ## Delete only those edges that have key01 as head.
            for tail in self.entries.keys():
                ## Delete this edge tail-->key01.
                self.deleteSingleDirectedEdge(tail,key01)
            return True
        else:
            ## Graph undirected.  So delete 
            ## all incident edges.
            return self.deleteEdgesIncidentOnTail(key01)

        # Does not exist, return False;
        return False

    # ------------------------------------------
    # Validated.
    def deleteAllEdgesIncidentOn(self,key01):
        """ Delete all edges, directed or undirected, that
            are incident on node key01, for key01
            both a head and a tail.
            key01 should no longer be in graph.
            Returns True if insert successful;
            False otherwise.
        """

        if self.isdirected:
            self.deleteEdgesIncidentOnHead(key01)
            self.deleteEdgesIncidentOnTail(key01)
            return True

        else:
            ## Undirected graph.
            return self.deleteEdgesIncidentOnTail(key01)


    # ------------------------------------------
    # Validated.
    def deleteEdge(self,key01,key02):
        """ Delete an entry (edge) based on (key01,key02).
            For directed graph, delete edge key01-->key02.
            For undirected graph, delete both edges key01 <--> key02.
            Return True if (key01,key02) and entries deleted;
            False otherwise.
        """
        if (self.isdirected):
            self.deleteSingleDirectedEdge(key01,key02)
        else:
            self.deleteSingleDirectedEdge(key01,key02)
            self.deleteSingleDirectedEdge(key02,key01)
        

    # ------------------------------------------
    # Validated.
    def deleteIsolatedNode(self,key01):
        """ Delete an isolated node, key01.
            Return True if (key01,key02) and entries deleted;
            False otherwise.
        """
        issucc=False
        if key01 in self.isolated:
            issucc=True

        self.isolated.remove(key01)
        return(issucc)
        
    # ------------------------------------------
    # Validated.
    def clear(self):
        """ Clear the entire graph/map/data structure.
        """
        self.entries.clear()
        self.isolated.clear()
    

    # ------------------------------------------
    def getAllTailNodes(self):
        """ Return list of all the nodes that appear in
            graph as tail nodes; i.e., nodes A 
            such that A --> head.
        """
        ### return list(self.entries.keys())
        return self.entries.keys()


    # ------------------------------------------
    def getAllHeadNodes(self):
        """ Return all the nodes that appear in
            graph as head nodes; i.e., nodes A 
            such that edges are tail --> A.
        """
        headSet = set()
        for tail in self.entries.keys():
            locHeads = self.entries[tail]
            for head in locHeads:
                headSet.add(head)
        myList = list(headSet)
        headSet.clear()
        ### return list(headSet)
        return myList


    # ------------------------------------------
    def getAllHeadNodesSet(self):
        """ Return all the nodes that appear in
            graph as head nodes; i.e., nodes A 
            such that edges are tail --> A.
        """
        headSet = set()
        for tail in self.entries.keys():
            locHeads = self.entries[tail]
            for head in locHeads:
                headSet.add(head)
        return headSet

    # ------------------------------------------
    # Verified.
    def getIsolatedNodes(self):
        """ Return all the isolated nodes.
        """
        return self.isolated
    
    # ------------------------------------------
    ## Verify.
    def getNodesIntoKey01(self,key01):
        """ 
            Return the list A of nodes (keys)
            such that there are edges A --> key01.
        """
        if not self.isdirected:
            ## Undirected.
            ## Note:  this list is a reference.
            ## Since undirected, we can use edges key01-->A,
            ## the reverse edges.
            if key01 in self.entries:
                ## So head nodes of key01 are also
                ## tail nodes with edges to A.
                return list(self.entries[key01])
            else:
                return list()
        else:
            ## Note this list is new.
            outList = list()
            for tail in self.entries:
                heads = self.entries[tail]
                if key01 in heads:
                    outList.append(tail)
            return outList
        

    # ------------------------------------------
    ## Verify.
    def getNodesOutOfKey01(self,key01):
        """ 
            Return the list B of nodes (keys)
            B such that key01 --> B.
        """
        ## Undirected.
        ## Note:  this list is a reference.
        ## Since undirected, we can use edges key01-->A,
        ## the reverse edges.
        if key01 in self.entries:
            ## So head nodes of key01 are also
            ## tail nodes with edges to A.
            return list(self.entries[key01])
        else:
            return list()

    # ------------------------------------------
    def getNeighborsProvided(self,key01,setNbrs):
        """ 
            Return the list B of nodes (keys)
            B such that key01 --> B.
        """
        ## Get set of all A's such that edges key01-->A,
        if key01 in self.entries.keys():
            theref = self.entries[key01]
            for item in theref:
                setNbrs.add(item)
            

        

    # ------------------------------------------
    ## Verify.
    def getNeighbors(self,key01):
        """ 
            Return the set B of nodes (keys)
            B such that key01 --> B.
        """
        ## Get set of all A's such that edges key01-->A,
        if key01 in self.entries:
            return self.entries[key01]
        else:
            newSet = set()
            return newSet


    # ------------------------------------------
    # Verified.
    def getNodeOutDegree(self,key01):
        """ 
            Return the out-degree of key01; 
            cardinality of B such that key01 --> B.

        """
        if key01 in self.entries:
            ## So head nodes of key01 are also
            ## tail nodes with edges to A.
            return len(self.entries[key01])
        else:
            return 0


    # ------------------------------------------
    # Verified.
    def getNodeInDegree(self,key01):
        """ 
            Return the number of nodes (keys)
            A such that A --> key01.

        """
        if not self.isdirected:
            ## Undirected.
            ## Note:  this list is a reference.
            ## Since undirected, we can use edges key01-->A,
            ## the reverse edges.
            if key01 in self.entries:
                ## So head nodes of key01 are also
                ## tail nodes with edges to A.
                return len(self.entries[key01])
            else:
                return 0
        else:
            ## Note this list is new.
            outList = list()
            for tail in self.entries:
                heads = self.entries[tail]
                if key01 in heads:
                    outList.append(tail)
            numNodes = len(outList)
            del outList[:]
            return numNodes
        
    # ------------------------------------------
    # Verified.
    def getNodeDegree(self,key01):
        """ 
            This is for undirected graphs only.
            Return the degree (=in-degree=out-degree) of node key01; 
        """
        return self.getNodeOutDegree(key01)



    # ------------------------------------------
    # Verified.
    def addDirectedEdge(self,key01,key02):
        """ Add directed edge key01 --> key02.
            Add only if edge does not exist.
            This only adds edge key01-->key02, irrespective
            of whether graph is directed.
            Be careful:  typically only use this method
            if graph is directed.
            Returns True if insert successful;
            False otherwise.
        """

        ## issucc = True if add is successful.
        issucc=False
        if key01 == key02:
            ## No self loops.
            ### return False
            issucc=False
        
        elif self.hasEdge(key01,key02):
            ### return False
            issucc=False

        elif self.hasTail(key01):
            self.entries[key01].add(key02)
            ### return True
            issucc=True

        else:
            heads = set()
            heads.add(key02)
            self.entries[key01]=heads
            ### return True
            issucc=True

        if issucc:
            if key01 in self.isolated:
                self.isolated.remove(key01)
            if key02 in self.isolated:
                self.isolated.remove(key02)
             
        return issucc

 
    # ------------------------------------------
    # Verified.
    def addUndirectedEdge(self,key01,key02):
        """ Add directed edge key01 --> key02.
            Add only if edge does not exist.
            This only adds edge key01-->key02, irrespective
            of whether graph is directed.
            Returns True if insert successful;
            False otherwise.
        """

        issucc01=False
        issucc02=False
        if self.isdirected:
            ## Cannot add an undirected edge to a directed graph.
            ### return False
            ### print "isdirected flag problem."
            issucc01=False

        elif key01 == key02:
            ## No self loops.
            ### return False
            ### print "self loop problem."
            issucc01=False

        elif (  (not self.hasEdge(key01,key02)) and (not self.hasEdge(key02,key01))  ):
            ## Neither edge exists, so add both.
            issucc01 = self.addDirectedEdge(key01,key02)
            ### print " issucc01: ",issucc01
            issucc02 = self.addDirectedEdge(key02,key01)
            ### print " issucc02: ",issucc02
            if (not issucc01) or (not issucc02):
                print("Error.   We try to add an undirected edges (",key01,",",key02,").")
                print("    The edge does not previously exist, so should be added.")
                print("    But the insertion failed, meaning graph is out of synch.")
                print("Terminate.")
                quit()
        #### else:
        ####     ### return False
        ####    print " else False"
        ####     issucc01=False

        if issucc01 and issucc02:
            ## Adding edge(s) was successful; so check to see if used an isolated node.
            if key01 in self.isolated:
                self.isolated.remove(key01)
            if key02 in self.isolated:
                self.isolated.remove(key02)
             
        return issucc01
        

    # ------------------------------------------
    # Verified.
    def addEdge(self,key01,key02):
        """ Add an edge to a graph.  The directionality
            is taken into account.
        """
        if self.isdirected:
            return self.addDirectedEdge(key01,key02)
        else:
            return self.addUndirectedEdge(key01,key02)


    
    # ------------------------------------------
    # Verified.
    def addNode(self,key01):
        """ Add a single node to the graph.
            Add only if it does not exist.
            Returns True if insert successful;
            False otherwise.
        """

        if self.hasNode(key01):
            return False

        else:
            self.isolated.add(key01)
            return True
        

    # ------------------------------------------
    # Verified.
    def getNodesList(self):
        """ Get all nodes and return as a list.
        """
        theset = self.getNodesSet() 
        thelist = list( theset )
        theset.clear()
        return thelist

    # ------------------------------------------
    # Verified.
    def getNodesSet(self):
        """ Get all nodes and return as a set.
        """
        s_nodes = set()

        for tail in self.entries.keys():
            s_nodes.add(tail)
            heads = self.entries[tail]
            for head in heads:
                s_nodes.add(head)

        for iso in self.isolated:
            s_nodes.add(iso)

        return s_nodes

    # ------------------------------------------
    def getNodesSetProvided(self,s_nodes):
        """ Get all nodes and return as a set.
        """

        for tail in self.entries.keys():
            s_nodes.add(tail)
            heads = self.entries[tail]
            for head in heads:
                s_nodes.add(head)

        for iso in self.isolated:
            s_nodes.add(iso)

        return


    # ------------------------------------------
    # Verified.
    def getNumNodes(self):
        """ Get num nodes.
        """
        return  len( self.getNodesSet()  )

    # ------------------------------------------
    # Verified. 
    def getNumHeadNodes(self):
        """ Get num head nodes.
        """
        return  len( self.getAllHeadNodes()  )


    # ------------------------------------------
    # Verified.
    def getNumTailNodes(self):
        """ Get num tail nodes.
        """
        return  len( self.getAllTailNodes()  )

    # ------------------------------------------
    # Verified.
    def getNumIsolatedNodes(self):
        """ Get num isolated tail nodes.
        """
        return  len( self.getIsolatedNodes()  )


    # ------------------------------------------
    # Verified.
    def getNumEdges(self):
        """ Get num edges.
        """

        numEdges=0

        for tail in self.entries.keys():
            heads = self.entries[tail]
            numEdges += len(heads)
        if self.isdirected:
            return  numEdges
        else:
            return (numEdges/2)


    # ------------------------------------------
    # Verified.
    def isEmpty(self):
        """ Get num tail nodes.
        """

        ## Cannot count edges because can be isolated 
        ## nodes.
        return  self.getNumNodes() == 0 


    # ------------------------------------------
    # Verified.
    def deepCopy(self,lgSource):
        """ Copy everything from lgSource (a LeanGraph)
            into this graph.
        """

        ### Copy in isolatedNodes.
        sourceSet = lgSource.getIsolatedNodes() 
        for item in sourceSet:
            self.isolated.add(item)

        ### Copy edges.
        tailList = lgSource.getAllTailNodes()
        for tail in tailList:
            headList = lgSource.getNodesOutOfKey01(tail)
            for head in headList:
                self.addDirectedEdge(tail,head)
        
        return



    # ------------------------------------------
    # Verified.
    def printContentsAdjList(self):
        """ Print contents of graph.
        """
        if (self.isEmpty()):
            print("  --- empty data structure ---")
            return

        for key01 in sorted(self.entries.iterkeys()):
            heads = self.entries[key01]
            print(key01,len(heads))
            for key02 in heads:
                print("    ",key02)

        if len(self.isolated) > 0:
            print(" isolated nodes: ",self.isolated)

    # ------------------------------------------
    # Validated.
    def readUelFromFile(self,filename,kind=1):
        """  Read a file in UEL format.
             
             Arguments:
                   filename:  file containing graph.
                   kind:  1 means read node IDs as strings; !=1 means integers.

             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.
        """
        if (not os.path.exists(filename)):
            # File does not exist.
            return False, 1, "Failed first IF statement in readUelFromFile(); file does not exist."
        if (not os.path.isfile(filename)):
            # Name is not a file.
            return False, 2, "Failed second IF statement in readUelFromFile()"
        # Open.
        fh = open(filename,"r")
        (bool,code,line) = self.readUelFromHandle(fh,kind)
        fh.close()
        return bool,code,line

    # ------------------------------------------
    # Validated.
    def readUelFromHandle(self,fhandle,kind):
        """  Read a file in UEL format.

             Modified to read any number of numbers on a line, but only 
             use the first 2.
             The graph is simple: no self loops.

             Arguments:
                   fhandle:  file handle.
                   kind:  1 means read node IDs as strings; !=1 means integers.

             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.

             Can read a single node on a line.
        """

        # Read first line.
        line = fhandle.readline()
        line = line.strip()
        while (len(line) > 0):
            # Split a line into tokens; space delimitor.
            # List is zero indexed.
            listTokens = line.split() 

            ###  if (len(listTokens) != 1) and (len(listTokens) != 2):
            ###      # Need one or two values for the vertices of edge.
            ###      return False,3,line
            
            ## No self loops.
            if kind == 1:
                key01 = listTokens[0]
                ### if len(listTokens) == 2:
                if len(listTokens) >= 2:
                    key02 = listTokens[1]
                    if key01 == key02:
                        continue
            else:
                key01 = int(listTokens[0])
                ### if len(listTokens) == 2:
                if len(listTokens) >= 2:
                    key02 = int(listTokens[1])
                    if key01 == key02:
                        continue

            ### if len(listTokens) == 2:
            if len(listTokens) >= 2:
                if self.isdirected: 
                    issucc = self.addDirectedEdge(key01,key02)
                    if not issucc:
                        return False, 16, line
                else:
                    issucc = self.addUndirectedEdge(key01,key02)
                    ### print " ----> issucc: ",issucc
                    if not issucc:
                        return False, 17, line
            else:
                ## Isolated node.
                if kind == 1:
                    key01 = listTokens[0]
                else:
                    key01 = int(listTokens[0])

                ##-x- print " key01: ",key01
                ##-x- print " hasNode(key01): ",self.hasNode(key01)
                ##-x- self.printContentsAdjList()

                if not self.hasNode(key01):
                    ## Node not in graph, so add it.
                    issucc = self.addNode(key01)
                    if not issucc:
                        return False, 18, line

            ## Read next line.
            line = fhandle.readline()
            line = line.strip()

        return True,0,line

    # ------------------------------------------
    # Verified.
    def readDelFromFile(self,filename,kind=1):
        """  Read a file in DEL (directed edge list) format.
             
             Arguments:
                   filename:  file containing graph.
                   kind:  1 means read node IDs as strings; !=1 means integers.

             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.
        """
        return self.readUelFromFile(filename,kind)


    # ------------------------------------------
    # Verified.
    def readDelFromHandle(self,fhandle,kind):
        """  Read a file in DEL format.

             Arguments:
                   fhandle:  file handle.

             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.
        """
        return readUelFromHandle(fhandle,kind)




    # ------------------------------------------
    # Verified.
    def writeUelToFile(self,filename,overwrite=0):
        """  Write contents to file in UEL format.
             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.

             Attributes:
                filename:  output file to write data.
                overwrite:  If 0, will NOT overwrite existing file.
                            Else, will overwrite.
                            
        """
        if (overwrite==0):
            if (os.path.exists(filename)):
                # File already exists and
                # we are not to overwrite.
                return False, 1
        # Open.
        fh = open(filename,"w")
        (bool,code) = self.writeUelToHandle(fh)
        fh.close()
        return bool,code


    # ------------------------------------------
    # Verified.
    def writeUelToHandle(self,fhandle):
        """  Write a file in UEL or DEL format.
             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.

             Attributes:
                fhandle:  handle to write data.
        """
        # Sort the keys for reproducible output.
        if self.isdirected:
             ## Directed graph.
            for key01 in sorted(self.entries.iterkeys()):
                heads = self.entries[key01]
                # Sort the keys for reproducible output.
                ### heads.sort()
                for key02 in heads:
                    fhandle.write(str(key01) + " " + str(key02) + "\n")
        else:
            ## Undirected graph.
            for key01 in sorted(self.entries.iterkeys()):
                heads = self.entries[key01]
                # Sort the keys for reproducible output.
                ### heads.sort()
                for key02 in heads:
                    if (key01 < key02):
                        fhandle.write(str(key01) + " " + str(key02) + "\n")
                     
        return True,0

    # ------------------------------------------
    # Verified.
    def writeDelToFile(self,filename,overwrite=0):
        """  Write contents to file in DEL format.
             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.

             Attributes:
                filename:  output file to write data.
                overwrite:  If 0, will NOT overwrite existing file.
                            Else, will overwrite.
                            
        """
        ## Edge directionality taken care of internally.
        return self.writeUelToFile(filename,overwrite)


    # ------------------------------------------
    # Verified.
    def writeDelToHandle(self,fhandle):
        """  Write a DEL to file.
             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.

             Attributes:
                fhandle:  handle to write data.
        """
        ## Even though this is write "UEL", it handles DEL,
        ## based on isDirected attribute.
        return self.writeUelToHandle(fhandle)



    # ------------------------------------------
    # Validated.
    def readAdjListFromFile(self,filename,readingHalf,kind):
        """  Read a file in an adjacency list format.
             filename:  name of file to read from.
             readingHalf:  if 1, then for each line read
                           in, two edges are created.
                           if 0, then each line (edge)
                           of file produces one edge in graph.
             kind:         1 means keep node IDs as strings.
                           otherwise, convert IDs to ints.
             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.
        """
        if (not os.path.exists(filename)):
            # File does not exist.
            return False, 1, "nothing"
        if (not os.path.isfile(filename)):
            # Name is not a file.
            return False, 2, "nothing"
        # Open.
        fh = open(filename,"r")
        (bool,code,line) = self.readAdjListFromHandle(fh,readingHalf,kind)
        fh.close()
        return bool,code,line

    # ------------------------------------------
    # Validated.
    def readAdjListFromHandle(self,fhandle,readingHalf,kind):
        """  Read a file in AdjList format.

             Graph is simple; no self loops.

             fhandle:      handle of file to read from.
             readingHalf:  if 1, then for each line read
                           in, two edges are created.
                           if 0, then each line (edge)
                           of file produces one edge in graph.
             kind:         1 means keep node IDs as strings.
                           otherwise, convert IDs to ints.
             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.
        """
        # Read first line.
        line = fhandle.readline()
        line = line.strip()
        while (len(line) > 0):
            # Split a line into tokens; space delimitor.
            # List is zero indexed.
            listTokens = line.split() 

            if (len(listTokens) != 2):
                # Need one node and its degree.
                print("Line that is bad: ",line)
                return False,3,line
            key01 = listTokens[0]
            if kind == 1:
                key01 = listTokens[0]
            else:
                key01 = int(listTokens[0])

            degree = int(listTokens[1])
            for ideg in range(0,degree):
                line = fhandle.readline()
                # print "line: ",line
                listTokens = line.split()
                key02 = listTokens[0]

                ### print "key01  degree key02: ",key01,degree,key02

                if kind == 1:
                    key02 = listTokens[0]
                else:
                    key02 = int(listTokens[0])
                
                if key01 == key02:
                    ## No self loops.
                    continue

                if self.isdirected:
                    if readingHalf==1:
                        ## Reading only 1/2 of data
                        ## so store 2 edges.
                        self.addDirectedEdge(key01,key02)
                        self.addDirectedEdge(key02,key01)
                    else:
                        ## Read one for one.
                        ## This is edge key01-->key02.
                        self.addDirectedEdge(key01,key02)
                else:
                    ## Undirected graph.
                    if readingHalf==1:
                        ## Reading only 1/2 of data
                        ## so store 2 edges.
                        ## Reading a b. So create a-->b and b-->a.
                        self.addUndirectedEdge(key01,key02)
                    else:
                        ## Read one for one.
                        ## This will probably not be used much.
                        ## This is edge key01-->key02.
                        self.addDirectedEdge(key01,key02)

            ##-x- print " key01: ",key01
            ##-x- print " hasNode(key01): ",self.hasNode(key01)
            ##-x- self.printContentsAdjList()

            if (degree == 0):
                ## Just add a node.
                if not self.hasNode(key01):
                    ## Node not in graph, so add it.
                    issucc = self.addNode(key01)
                    if not issucc:
                        return False, 182,line

            # Read next line. 
            line = fhandle.readline()
            line = line.strip()

        return True,0,line

    # ------------------------------------------
    def readReverseAdjListFromFile(self,filename,readingHalf,kind):
        """  Read a file in an adjacency list format.
             Store an edge that is in a file (tail,head) as
             (head,tail) in this code.  
             So the edge direction is reversed.
             This method only makes sense when 
             readingHalf=0.
             filename:  name of file to read from.
             readingHalf:  if 1, then for each line read
                           in, two edges are created.
                           if 0, then each line (edge)
                           of file produces one edge in graph.
             kind:         1 means keep node IDs as strings.
                           otherwise, convert IDs to ints.
             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.
        """
        if (not os.path.exists(filename)):
            # File does not exist.
            return False, 1, "nothing"
        if (not os.path.isfile(filename)):
            # Name is not a file.
            return False, 2, "nothing"
        # Open.
        fh = open(filename,"r")
        (bool,code,line) = self.readReverseAdjListFromHandle(fh,readingHalf,kind)
        fh.close()
        return bool,code,line


    # ------------------------------------------
    def readReverseAdjListFromHandle(self,fhandle,readingHalf,kind):
        """  Read a file in AdjList format.
             But the edges are flipped; i.e., a an edge in an
             input file (a,b) is stored herein as (b,a).

             Graph is simple; no self loops.

             fhandle:      handle of file to read from.
             readingHalf:  if 1, then for each line read
                           in, two edges are created.
                           if 0, then each line (edge)
                           of file produces one edge in graph.
             kind:         1 means keep node IDs as strings.
                           otherwise, convert IDs to ints.
             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.
        """
        # Read first line.
        line = fhandle.readline()
        line = line.strip()
        while (len(line) > 0):
            # Split a line into tokens; space delimitor.
            # List is zero indexed.
            listTokens = line.split() 

            if (len(listTokens) != 2):
                # Need one node and its degree.
                print("Line that is bad: ",line)
                return False,3,line
            key01 = listTokens[0]
            if kind == 1:
                key01 = listTokens[0]
            else:
                key01 = int(listTokens[0])

            degree = int(listTokens[1])
            for ideg in range(0,degree):
                line = fhandle.readline()
                # print "line: ",line
                listTokens = line.split()
                key02 = listTokens[0]

                ### print "key01  degree key02: ",key01,degree,key02

                if kind == 1:
                    key02 = listTokens[0]
                else:
                    key02 = int(listTokens[0])
                
                if key01 == key02:
                    ## No self loops.
                    continue

                if self.isdirected:
                    if readingHalf==1:
                        ## Reading only 1/2 of data
                        ## so store 2 edges.
                        self.addDirectedEdge(key01,key02)
                        self.addDirectedEdge(key02,key01)
                    else:
                        ## Read one for one.
                        ## This is the edge that is in file: key01-->key02.
                        ## But here is where we reverse direction,
                        ## storing as key02-->key01.
                        self.addDirectedEdge(key02,key01)
                else:
                    ## Undirected graph.
                    if readingHalf==1:
                        ## Reading only 1/2 of data
                        ## so store 2 edges.
                        ## Reading a b. So create a-->b and b-->a.
                        self.addUndirectedEdge(key01,key02)
                    else:
                        ## Read one for one.
                        ## This will probably not be used much.
                        ## This is the edge that is in file: key01-->key02.
                        ## But here is where we reverse direction,
                        ## storing as key02-->key01.
                        self.addDirectedEdge(key02,key01)

            ##-x- print " key01: ",key01
            ##-x- print " hasNode(key01): ",self.hasNode(key01)
            ##-x- self.printContentsAdjList()

            if (degree == 0):
                ## Just add a node.
                if not self.hasNode(key01):
                    ## Node not in graph, so add it.
                    issucc = self.addNode(key01)
                    if not issucc:
                        return False, 182,line

            # Read next line. 
            line = fhandle.readline()
            line = line.strip()

        return True,0,line

    # ------------------------------------------
    # Validated.
    def writeAdjListToFile(self,filename,overwrite=1,writeHalf=0):
        """  Write contents to file in AL format.
             This is for both directed and undirected graphs.
             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.

             Attributes:
                filename:  output file to write data.
                overwrite:  If 0, will NOT overwrite existing file.
                            Else, will overwrite.
                writeHalf:  This works irrespective of undirected or directed graph. 
                            If 0, will write all contents
                            of this object to file.
                            So for an undirected graph, and edge  a<-->b,
                            it will write a-->b and b-->a.
                            Else (i.e., if writeHalf=1), will write (a,b) only 
                            when a<b.
                            So if the graph is directed, one wants writeHalf=0.
                            writeHalf=1 mainly for undirected graphs.
                            
        """
        if (overwrite==0):
            if (os.path.exists(filename)):
                # File already exists and
                # we are not to overwrite.
                return False, 1
        # Open.
        fh = open(filename,"w")
        (bool,code) = self.writeAdjListToHandle(fh,writeHalf)
        fh.close()
        return bool,code

    # ------------------------------------------
    # Validated.
    def writeAdjListToHandle(self,fhandle,writeHalf=0):
        """  Write an adjacency list representation of 
             graph to file.

             Return is (True,0) if successful.
             Return is (False, num) if error.
             Error codes are below.

             Attributes:
                filename:  output file to write data.
                writeHalf:  This works irrespective of undirected or directed graph. 
                            If 0, will write all contents
                            of this object to file.
                            So for an undirected graph, and edge  a<-->b,
                            it will write a-->b and b-->a.
                            Else (i.e., if writeHalf=1), will write (a,b) only 
                            when a<b.
                            So if the graph is directed, one wants writeHalf=0.
                            writeHalf=1 mainly for undirected graphs.
                            
        """
        # Sort the keys for reproducible output.
        ## This is for edges.
        for key01 in sorted(self.entries.iterkeys()):
            heads = self.entries[key01]
            headsList = list(heads)
            headsList.sort()
            degree = len(heads)

            if (writeHalf ==0):
                # Write all edges.
                fhandle.write(str(key01) + " " + str(degree) + "\n")
                for head in headsList:
                    fhandle.write("   " + str(head) + "\n")

            else:
                ## Must recalculate degree.
                degree=0
                for head in headsList:
                    if (key01 < head):
                        degree+=1
                if degree>0:
                    fhandle.write(str(key01) + " " + str(degree) + "\n")
                    for head in heads:
                        if (key01 < head):
                            fhandle.write("   " + str(head) + "\n")

        ## This is for isolated nodes.
        for key01 in self.isolated:
            fhandle.write(str(key01) + " 0\n")
   

        return True,0


    # ------------------------------------------
    def keepOnlyMutualEdges(self):
        """ 
        This is ONLY for an directed graph.
        Removes all the key pairs that are not bidirectional. 
        In other words,
        suppose edge a-->b exists, but not edge b-->a.
        In this case, a-->b is deleted.
        If a-->b exists and b-->a exists, then keep both edges.
        This effectively converts the map into an undirected graph that is a subgraph of
        the original directed graph

        Return   True if process completed; False otherwise.
        """
        #If this is an undirected graph, all links are bidirectional
        if not self.isdirected:
           return False
           
        for key01 in self.entries.keys():
            heads = self.entries[key01]
            for key02 in heads:
                if (key02 not in self.entries) or (key01 not in self.entries[key02]):
                    ## We have edge key01-->key02, but not key02-->key01.
                    ## So delete key01-->key02.
                    self.deleteSingleDirectedEdge(key01,k02)
              
        return True


### ---------------------------------
### ---------------------------------
### From here on down, we are reading and writing multi-element node 
### graphs.
### Lets postpone for now.
### ---------------------------------
### ---------------------------------

##>>--      # ------------------------------------------
##>>--      # Read in a graph in UEL format from file.
##>>--      # Must specify the number labels to 
##>>--      # read (this is only for a check).
##>>--      # In reading from a file, values are always
##>>--      # lists.
##>>--      # Validated.
##>>--      def readMultiNodeUelFromFile(self,filename,numNodeElems,numLabels=-1):
##>>--          """  Read a file in UEL format.
##>>--               
##>>--               Arguments:
##>>--                     filename:  file containing graph.
##>>--                     numNodeElems:  number of entries for each node.
##>>--                     numLabels:  length of value list (if >=0)
##>>--  
##>>--               Return is (True,0) if successful.
##>>--               Return is (False, num) if error.
##>>--               Error codes are below.
##>>--          """
##>>--          if (not os.path.exists(filename)):
##>>--              # File does not exist.
##>>--              return False, 1
##>>--          if (not os.path.isfile(filename)):
##>>--              # Name is not a file.
##>>--              return False, 2
##>>--          # Open.
##>>--          fh = open(filename,"r")
##>>--          (bool,code) = self.readMultiNodeUelFromHandle(fh,numNodeElems,numLabels)
##>>--          fh.close()
##>>--          return bool,code
##>>--  
##>>--      # ------------------------------------------
##>>--      # Read in a graph in UEL format from existing handle.
##>>--      # Must specify the number labels to 
##>>--      # read (this is only for a check).
##>>--      # In reading from a file, values are always
##>>--      # lists.
##>>--      # Default for numLabels means do not check for
##>>--      # number of labels per line; so lines can have
##>>--      # different numbers of labels.
##>>--      # Validated.
##>>--      def readMultiNodeUelFromHandle(self,fhandle,numNodeElems,numLabels=-1):
##>>--          """  Read a file in UEL format.
##>>--  
##>>--               Arguments:
##>>--                     fhandle:  file handle.
##>>--                     numNodeElems:  number of entries for each node.
##>>--                     numLabels:  length of value list (if >=0)
##>>--  
##>>--               Return is (True,0) if successful.
##>>--               Return is (False, num) if error.
##>>--               Error codes are below.
##>>--          """
##>>--          # Read first line.
##>>--          line = fhandle.readline()
##>>--          while (len(line) > 0):
##>>--              # Split a line into tokens; space delimitor.
##>>--              # List is zero indexed.
##>>--              listTokens = line.split() 
##>>--  
##>>--              if (len(listTokens) < (2*numNodeElems)):
##>>--                  # Need numNodeElems for each node.
##>>--                  return False,3
##>>--              if (numLabels >= 0):
##>>--                  # Only check for proper number
##>>--                  # of labels if numLabels >=0.
##>>--                  if (len(listTokens) != (2*numNodeElems + numLabels)):
##>>--                      # Wrong number of labels for edge.
##>>--                      return False,4
##>>--              
##>>--              key01 = tuple(listTokens[0:numNodeElems])
##>>--              key02 = tuple(listTokens[numNodeElems:2*numNodeElems])
##>>--              # Inst. a new list.
##>>--              values=[]
##>>--              values = copy.deepcopy(listTokens[2*numNodeElems:])
##>>--              if (numLabels >=0):
##>>--                  # If numlabels < 0, do not check.
##>>--                  if (len(values) != numLabels):
##>>--                      return False,5
##>>--  
##>>--              # Set into dkmap.
##>>--              self.setValue(key01,key02,values)
##>>--              if (not self.isdirected):
##>>--                  # Undirected graph.  Add other edge.
##>>--                  self.setValue(key02,key01,values)
##>>--  
##>>--              # Read next line. 
##>>--              line = fhandle.readline()
##>>--          return True,0
##>>--  
##>>--      # ------------------------------------------
##>>--  
##>>--      # ------------------------------------------
##>>--      # Write data in UEL format to file.
##>>--      # Validated.
##>>--      def writeMultiNodeUelToFile(self,filename,overwrite=1,writeHalf=0):
##>>--          """  Write contents to file in UEL format.
##>>--               Return is (True,0) if successful.
##>>--               Return is (False, num) if error.
##>>--               Error codes are below.
##>>--  
##>>--               Attributes:
##>>--                  filename:  output file to write data.
##>>--                  overwrite:  If 0, will NOT overwrite existing file.
##>>--                              Else, will overwrite.
##>>--                  writeHalf:  If 0, will write all contents
##>>--                              of this object to file.
##>>--                              Else, will write (a,b) only 
##>>--                              when a<b.
##>>--                              If =1, then this write out
##>>--                              data for an undirected graph, say.
##>>--                              
##>>--          """
##>>--          if (overwrite==0):
##>>--              if (os.path.exists(filename)):
##>>--                  # File already exists and
##>>--                  # we are not to overwrite.
##>>--                  return False, 1
##>>--          # Open.
##>>--          fh = open(filename,"w")
##>>--          (bool,code) = self.writeMultiNodeUelToHandle(fh,writeHalf)
##>>--          fh.close()
##>>--          return bool,code
##>>--  
##>>--      # ------------------------------------------
##>>--      # Write data in UEL format to handle.
##>>--      # Validated.
##>>--      def writeMultiNodeUelToHandle(self,fhandle,writeHalf=0):
##>>--          """  Read a file in UEL format.
##>>--               Return is (True,0) if successful.
##>>--               Return is (False, num) if error.
##>>--               Error codes are below.
##>>--  
##>>--               Attributes:
##>>--                  filename:  output file to write data.
##>>--                  writeHalf:  If 0, will write all contents
##>>--                              of this object to file.
##>>--                              Else, will write (a,b) only 
##>>--                              when a<b.
##>>--                              If =1, then this write out
##>>--                              data for an undirected graph, say.
##>>--                              
##>>--          """
##>>--          # Sort the keys for reproducible output.
##>>--          for key01 in sorted(self.entries.iterkeys()):
##>>--              tempmap = self.entries[key01]
##>>--              # Sort the keys for reproducible output.
##>>--              for key02 in sorted(tempmap.iterkeys()):
##>>--                   
##>>--                  if (  (writeHalf ==0) or 
##>>--                        ((writeHalf !=0) and (key01 < key02))  ):
##>>--                      # If we are not to write half, or
##>>--                      # If we are to write half and key01 < key02.
##>>--  
##>>--                      for elem in key01:
##>>--                          fhandle.write(str(elem) + " " )
##>>--                      for elem in key02:
##>>--                          fhandle.write(str(elem) + " " )
##>>--   
##>>--                      (issucc,value)= self.getValue(key01,key02)
##>>--                      if (not issucc):
##>>--                           return False,0
##>>--                      fhandle.write(" ")
##>>--                      for num in value:
##>>--                          fhandle.write(" " + str(num))
##>>--                      fhandle.write('\n')
##>>--          
##>>--          return True,0
##>>--  
##>>--  
##>>--      # ------------------------------------------
##>>--      # Read in a graph in Adj List format from file.
##>>--      # Must specify the number labels to 
##>>--      # read (this is only for a check).
##>>--      # In reading from a file, values are always
##>>--      # lists.
##>>--      # Validated.
##>>--      def readMultiNodeAdjListFromFile(self,filename,numNodeElems,numLabels=-1):
##>>--          """  Read a file in UEL format.
##>>--               numNodeElems:   number of elements to a node.
##>>--               Return is (True,0) if successful.
##>>--               Return is (False, num) if error.
##>>--               Error codes are below.
##>>--          """
##>>--          if (not os.path.exists(filename)):
##>>--              # File does not exist.
##>>--              return False, 1
##>>--          if (not os.path.isfile(filename)):
##>>--              # Name is not a file.
##>>--              return False, 2
##>>--          # Open.
##>>--          fh = open(filename,"r")
##>>--          (bool,code) = self.readMultiNodeAdjListFromHandle(fh,numNodeElems,numLabels)
##>>--          fh.close()
##>>--          return bool,code
##>>--  
##>>--      # ------------------------------------------
##>>--      # Read in a graph in AL format from existing handle.
##>>--      # Must specify the number labels to 
##>>--      # read (this is only for a check).
##>>--      # In reading from a file, values are always
##>>--      # lists.
##>>--      # Default for numLabels means do not check for
##>>--      # number of labels per line; so lines can have
##>>--      # different numbers of labels.
##>>--      # Validated.
##>>--      def readMultiNodeAdjListFromHandle(self,fhandle,numNodeElems,numLabels=-1):
##>>--          """  Read a file in AdjList format.
##>>--               Return is (True,0) if successful.
##>>--               Return is (False, num) if error.
##>>--               Error codes are below.
##>>--          """
##>>--          # Read first line.
##>>--          line = fhandle.readline()
##>>--          while (len(line) > 0):
##>>--              # Split a line into tokens; space delimitor.
##>>--              # List is zero indexed.
##>>--              listTokens = line.split() 
##>>--  
##>>--              if (len(listTokens) < 2):
##>>--                  print "Line that is bad: ",line
##>>--                  return False,3
##>>--  
##>>--              key01 = tuple(listTokens[0:numNodeElems])
##>>--              degree = int(listTokens[numNodeElems])
##>>--              for ideg in range(0,degree):
##>>--                  line = fhandle.readline()
##>>--                  # print "line: ",line
##>>--                  listTokens = line.split()
##>>--                  key02 = tuple(listTokens[0:numNodeElems])
##>>--                  # New storage.
##>>--                  values=[]
##>>--                  values = copy.deepcopy(listTokens[numNodeElems:])
##>>--                  if (numLabels >=0):
##>>--                      # Only check for proper number
##>>--                      # of labels if numLabels >=0.
##>>--                      if (len(values) != numLabels):
##>>--                          return False, 5
##>>--                  self.setValue(key01,key02,values)
##>>--                  if (not self.isdirected):
##>>--                      # Undirected graph.  Add other edge.
##>>--                      self.setValue(key02,key01,values)
##>>--  
##>>--              # Read next line. 
##>>--              line = fhandle.readline()
##>>--          return True,0
##>>--  
##>>--      # ------------------------------------------
##>>--  
##>>--      # ------------------------------------------
##>>--      # Write data in adj list (AL) format to file.
##>>--      # Validated.
##>>--      def writeMultiNodeAdjListToFile(self,filename,overwrite=1,writeHalf=0):
##>>--          """  Write contents to file in AL format.
##>>--               Return is (True,0) if successful.
##>>--               Return is (False, num) if error.
##>>--               Error codes are below.
##>>--  
##>>--               Attributes:
##>>--                  filename:  output file to write data.
##>>--                  overwrite:  If 0, will NOT overwrite existing file.
##>>--                              Else, will overwrite.
##>>--                  writeHalf:  If 0, will write all contents
##>>--                              of this object to file.
##>>--                              Else, will write (a,b) only 
##>>--                              when a<b.
##>>--                              If =1, then this write out
##>>--                              data for an undirected graph, say.
##>>--                              
##>>--          """
##>>--          if (overwrite==0):
##>>--              if (os.path.exists(filename)):
##>>--                  # File already exists and
##>>--                  # we are not to overwrite.
##>>--                  return False, 1
##>>--          # Open.
##>>--          fh = open(filename,"w")
##>>--          (bool,code) = self.writeMultiNodeAdjListToHandle(fh,writeHalf)
##>>--          fh.close()
##>>--          return bool,code
##>>--  
##>>--      # ------------------------------------------
##>>--  
##>>--      # ------------------------------------------
##>>--      # Write data in AL format to handle.
##>>--      # Validated.
##>>--      def writeMultiNodeAdjListToHandle(self,fhandle,writeHalf=0):
##>>--          """  Read a file in UEL format.
##>>--               Return is (True,0) if successful.
##>>--               Return is (False, num) if error.
##>>--               Error codes are below.
##>>--  
##>>--               Attributes:
##>>--                  filename:  output file to write data.
##>>--                  writeHalf:  If 0, will write all contents
##>>--                              of this object to file.
##>>--                              Else, will write (a,b) only 
##>>--                              when a<b.
##>>--                              If =1, then this write out
##>>--                              data for an undirected graph, say.
##>>--                              
##>>--          """
##>>--          # Sort the keys for reproducible output.
##>>--          for key01 in sorted(self.entries.iterkeys()):
##>>--              tempmap = self.entries[key01]
##>>--              degree = len(tempmap)
##>>--  
##>>--              if (writeHalf !=0):
##>>--                  # Must possibly alter the degree because
##>>--                  # may not be writing all edges.
##>>--                  degree=0
##>>--                  for key02 in sorted(tempmap.iterkeys()):
##>>--                      if (key01 < key02):
##>>--                          degree+=1
##>>--  
##>>--              if (degree==0):
##>>--                  # Do not write data for a zero-degree node.
##>>--                  continue
##>>--  
##>>--              # fhandle.write(str(key01) + " " + str(len(tempmap)) + '\n')
##>>--              for item in key01:
##>>--                  fhandle.write(str(item) + " ")
##>>--                  ### cjk
##>>--                  #### fhandle.write(str(item).encode('utf-8') + " ")
##>>--              fhandle.write(str(degree) + '\n')
##>>--  
##>>--              # fhandle.write(" ")
##>>--              # fhandle.write(len(tempmap))
##>>--              # fhandle.write('\n')
##>>--              # Sort the keys for reproducible output.
##>>--              for key02 in sorted(tempmap.iterkeys()):
##>>--                   
##>>--                  if (  (writeHalf ==0) or 
##>>--                        ((writeHalf !=0) and (key01 < key02))  ):
##>>--                      # If we are not to write half, or
##>>--                      # If we are to write half and key01 < key02:
##>>--  
##>>--                      fhandle.write("      ")
##>>--                      for item in key02:
##>>--                          ### cjk.
##>>--                          ### fhandle.write(str(item).encode('utf-8') + " ")
##>>--                          fhandle.write(str(item) + " ")
##>>--                      (issucc,value)= self.getValue(key01,key02)
##>>--                      if (not issucc):
##>>--                          return False,0
##>>--                      fhandle.write(" ")
##>>--                      for num in value:
##>>--                          fhandle.write(" " + str(num))
##>>--                      fhandle.write('\n')
##>>--  
##>>--          return True,0
##>>--  
##>>--      # ------------------------------------------
##>>--  
##>>--  
##>>--  
##>>--      # ------------------------------------------
##>>--      # Write data in adj list (AL) format to file.
##>>--      # Validated.
##>>--      def writeMultiNodeAdjListToFileLineId(self,filename,overwrite=1,writeHalf=0):
##>>--          """  Write contents to file in AL format.
##>>--               Return is (True,0) if successful.
##>>--               Return is (False, num) if error.
##>>--               Error codes are below.
##>>--  
##>>--               Attributes:
##>>--                  filename:  output file to write data.
##>>--                  overwrite:  If 0, will NOT overwrite existing file.
##>>--                              Else, will overwrite.
##>>--                  writeHalf:  If 0, will write all contents
##>>--                              of this object to file.
##>>--                              Else, will write (a,b) only 
##>>--                              when a<b.
##>>--                              If =1, then this write out
##>>--                              data for an undirected graph, say.
##>>--                              
##>>--          """
##>>--          if (overwrite==0):
##>>--              if (os.path.exists(filename)):
##>>--                  # File already exists and
##>>--                  # we are not to overwrite.
##>>--                  return False, 1
##>>--          # Open.
##>>--          fh = open(filename,"w")
##>>--          (bool,code) = self.writeMultiNodeAdjListToHandleLineId(fh,writeHalf)
##>>--          fh.close()
##>>--          return bool,code
##>>--  
##>>--      # ------------------------------------------
##>>--  
##>>--      # ------------------------------------------
##>>--      # Write data in AL format to handle.
##>>--      # Validated.
##>>--      def writeMultiNodeAdjListToHandleLineId(self,fhandle,writeHalf=0):
##>>--          """  Read a file in UEL format.
##>>--               Return is (True,0) if successful.
##>>--               Return is (False, num) if error.
##>>--               Error codes are below.
##>>--  
##>>--               Attributes:
##>>--                  filename:  output file to write data.
##>>--                  writeHalf:  If 0, will write all contents
##>>--                              of this object to file.
##>>--                              Else, will write (a,b) only 
##>>--                              when a<b.
##>>--                              If =1, then this write out
##>>--                              data for an undirected graph, say.
##>>--                              
##>>--          """
##>>--          # Sort the keys for reproducible output.
##>>--          for key01 in sorted(self.entries.iterkeys()):
##>>--              tempmap = self.entries[key01]
##>>--              degree = len(tempmap)
##>>--  
##>>--              if (writeHalf !=0):
##>>--                  # Must possibly alter the degree because
##>>--                  # may not be writing all edges.
##>>--                  degree=0
##>>--                  for key02 in sorted(tempmap.iterkeys()):
##>>--                      if (key01 < key02):
##>>--                          degree+=1
##>>--  
##>>--              if (degree==0):
##>>--                  # Do not write data for a zero-degree node.
##>>--                  continue
##>>--  
##>>--              # fhandle.write(str(key01) + " " + str(len(tempmap)) + '\n')
##>>--              fhandle.write("t    ")
##>>--              for item in key01:
##>>--                  fhandle.write(str(item) + " ")
##>>--                  ### fhandle.write(str(item).encode('utf-8') + " ")
##>>--              fhandle.write(str(degree) + '\n')
##>>--  
##>>--              # fhandle.write(" ")
##>>--              # fhandle.write(len(tempmap))
##>>--              # fhandle.write('\n')
##>>--              # Sort the keys for reproducible output.
##>>--              for key02 in sorted(tempmap.iterkeys()):
##>>--                   
##>>--                  if (  (writeHalf ==0) or 
##>>--                        ((writeHalf !=0) and (key01 < key02))  ):
##>>--                      # If we are not to write half, or
##>>--                      # If we are to write half and key01 < key02:
##>>--  
##>>--                      fhandle.write("h    ")
##>>--                      fhandle.write("      ")
##>>--                      for item in key02:
##>>--                          ### fhandle.write(str(item).encode('utf-8') + " ")
##>>--                          fhandle.write(str(item) + " ")
##>>--                      (issucc,value)= self.getValue(key01,key02)
##>>--                      if (not issucc):
##>>--                          return False,0
##>>--                      fhandle.write(" ")
##>>--                      for num in value:
##>>--                          fhandle.write(" " + str(num))
##>>--                      fhandle.write('\n')
##>>--  
##>>--          return True,0
##>>--  
##>>--      # ------------------------------------------


# -----------------------------------------------
# End class.
