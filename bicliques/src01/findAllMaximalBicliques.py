# -*- coding:utf-8 -*-

### 30 Sep 2013.
### cjk:  
### purpose:  Find all maximal bicliques in an arbitary
###           undirected graph.
###           A minimum size q is specified such that the 
###           sum of the nodes in the two partite sets
###           must be strictly greater than q.

import os
import sys
import LeanGraph
import time
import copy

### This temporarily alters your python path so that 
### we can test our new modules.  This enables us to 
### test and run without permanently putting in a 
### bunch of clutter into the PYTHONPATH.
### This is the path to the SkMap.py.
### This needs to be modified per user.
### sys.path.append('/groups/NDSSL/projects/EMBERS/Soc/codes/dkmap/src')



### -----------------------------
# Function definition is here


### -----------------------------
# Validated.
def getNewIntersection(graph,nodeSet,interNbrSet):
    """Generate the set of nbrs that is the intersection of
       the sets of neighbors of all nodes in nodeSet.
       That is, interNbrSet is the common set of neighbors of all nodes
       in nodeSet.
       
       Inputs:  graph:  undirected graph.
                nodeSet:  set of nodes whose neighbor sets are intersected.
       Outputs: interNbrSet:  common set of neighbors of all nodes in nodeSet.
 
    """
    isfirst=True;
    for node in nodeSet:
        nbrs = undg.getNeighbors(node)
        if isfirst:
            ### Make a deep copy.
            for item in nbrs:
                interNbrSet.add(item)
            isfirst=False
        else:
            interNbrSet.intersection_update(nbrs)
    return
        
### -----------------------------
## Validated.
def getNewAdditionalIntersection(graph,nodeSet,existingInterNbrSet,nodeV,newInterNbrSet):
    """Given a set of nodes nodeSet and its common neighbors,
       get the resulting intersection for nodeSet and {v}.
       Use \gamma(nodeSet) = existingInterNbrSet to speed calculations.
       Inputs:  graph:  undirected graph.
                nodeSet:  set of nodes whose neighbor sets are intersected.
                existingInterNbrSet: common neighbors among nodes in nodeSet.
                nodeV:  additional node.
       Outputs: newInterNbrSet:  common set of neighbors of all nodes in (nodeSet plus nodeV).
 
    """
    setV = graph.getNeighbors(nodeV)
    newSet = existingInterNbrSet.intersection(setV)
    for item in newSet:
        newInterNbrSet.add(item)

    newSet.clear()
    setV=None
    return
        

### -----------------------------
## Validated.
def getGammaXplusv(graph,setX,gammaX,nodeV):
     """ 
        Main entrypoint to the algorithm to compute

     """
     nbrsV = graph.getNeighbors(nodeV)
     intersect = gammaX.intersection(nbrsV)
     return intersect 
     

### -----------------------------
## Validated.
def getNumGammaXplusv(graph,setX,gammaX,nodeV):
     """ 
        Return the number of nodes in \gamma(setX \cup nodeV).

     """
     intersect = getGammaXplusv(graph,setX,gammaX,nodeV)
     thesize = len(intersect)
     intersect.clear()
     return thesize


### -----------------------------
def mineLmbc(graph,setX,gammaX,tailX,minNss,fhout):
     """ 
        Main entrypoint to the algorithm to compute
        maximum bicliques.  This is a recursive call.

        Inputs:  
              graph:  the undirected graph in which bicliques are sought.
              setX:  set of nodes that is a candidate partite set.
              gammaX:  The set of common nbrs of all nodes in setX.
              tailX:  set of nodes in graph that are lexigraphically larger than
                      those in setX, and are candidates to add to setX.
              minNss:  minimum size of the sum of nodes in partite sets of biclique.
              fhout:   output device for printing bicliques.
              
     """

     ## Remove all entries from list tailX that generate
     ## thesize < minNss.  Use sets.
     tempSet = set(tailX)
     toDelete = set()
     del tailX[:]
     for tail in tempSet:
         thesize = getNumGammaXplusv(graph,setX,gammaX,tail)
         if thesize < minNss:
             toDelete.add(tail)

     for item in toDelete:
         tempSet.remove(item)

     tailX = list( tempSet )
     tempSet.clear()
     toDelete.clear()

     if len(setX) + len(tailX) < minNss:
         return
         
     ## Sort all vertices v \in tailX in ascending order of |\gamma(X \cup v)|.
     mapCount = dict()
     for wnode in tailX:
         numInter = getNumGammaXplusv(graph,setX,gammaX,wnode)
         if numInter in mapCount:
             setN01 = mapCount[numInter]
             setN01.add(wnode)
         else:
             setN01 = set()
             setN01.add(wnode)
             mapCount[numInter]=setN01

     listSortedKeys = sorted(mapCount.keys())   
     sortedTailX = list()
     for thekey in listSortedKeys:
         theset = mapCount[thekey]
         for item in theset:
             sortedTailX.append(item)

     mapCount.clear()

     # Now operate on each vertex v of listSortedNodes.
     mylimit = len(sortedTailX)
     for idx in range(0,mylimit):
         focNode = sortedTailX[0]
         del sortedTailX[0] 
         if focNode in setX:
             print("Error.  the focNode should not be in setX because focNode is in tailX.")
             print("focNode: ",focNode)
             quit()
         if ( len(setX) + 1 + len(sortedTailX) ) >= minNss:
             ## oneSet is \gamma(setX \cup focNode).
             oneSet = set()
             getNewAdditionalIntersection(graph,setX,gammaX,focNode,oneSet)
             ## setY is \gamma(oneSet).
             setY = set()
             getNewIntersection(graph,oneSet,setY)
             setV = set()
             setV.add(focNode)
             set02 = setY.difference( setX.union(setV)  )
             tempSet02 = set(  sortedTailX   )
             issucc = set02.issubset(tempSet02)
             tempSet02.clear()
             set02.clear()
             setV.clear()
             if issucc:
                 if len(setY) >= minNss:
                     for itemY in setY:
                         fhout.write(" " + str(itemY))
                     for itemZ in oneSet:
                         fhout.write(" " + str(itemZ))
                     fhout.write("\n")
                 setAZ = set(sortedTailX)
                 setAZ.difference_update(setY)
                 listBZ = list(setAZ)
                 setAZ.clear()
                 ## Recursion.
                 mineLmbc(graph,setY,oneSet,listBZ,minNss,fhout)
           





### -----------------------------
### Start program.


if len(sys.argv) != 5:
    print("usage: exec infile graphFormat minNumNodesInSets outfile.")
    quit()


### Command line arguments.
infile = sys.argv[1]
## gformat =1 means UEL; gformat=2 means AdjList.
gformat = int(sys.argv[2])
minNss = int(sys.argv[3])
outfile = sys.argv[4]

print(" --------- echo inputs -------------")
print("infile: ",infile)
print("gformat: ",gformat)
print("minNss: ",minNss)
print("outfile: ",outfile)
print(" --------- echo inputs -------------")


startTime = time.time()


## -------------------------------
### Undirected graph.
undg = LeanGraph.LeanGraph(0)

if gformat==1:
    ## 0 means read node IDs as integers.
    (issucc, code, line) = undg.readUelFromFile(infile,0)
    if not issucc:
        print("Error in reading graph from UEL file.")
        print("error code: ",code)
        print("Failure line: ",line)
        print("terminate.")
        quit()
else:
    ## 1 means one line is one (directed) edge.
    ## 0 means read node IDs as integers.
    (issucc,code, line) = undg.readAdjListFromFile(infile,0,0)
    if not issucc:
        print("Error in reading graph from UEL file.")
        print("error code: ",code)
        print("Failure line: ",line)
        print("terminate.")
        quit()

endTime = time.time()
print(" Elapsed time to read in graph (s): ",endTime-startTime)
print(" graph read in; num nodes, edges: ",undg.getNumNodes(), undg.getNumEdges())


## -------------------------------
### Instantiate.

### Set of nodes.
setX = set()
### Set of nodes that are the common
### neighbors of all nodese in setX.
### i.e., it is the intersection of 
### of all nbrs of all nodes in setX.
gammaX = set()

### List of vertices that come after the
### largest node ID in setX.
tailX = list()

## -------------------------------
## Initialize.
setX.clear()
undg.getNodesSetProvided(gammaX)
tailX = undg.getNodesList()

## Validation.
## print "setX: ",setX
## print "gammaX: ",gammaX
## print "tailX: ",tailX


###  ## -------------------------------
###  ## Primary recursive call.
fhout = open(outfile,"w")
mineLmbc(undg,setX,gammaX,tailX,minNss,fhout)


### -----------------------
### Validation.
###  interNbrSet = set()
###  nodeSet = set()
###  nodeSet.add(1)
###  nodeSet.add(5)
###  print " graph"
###  undg.printContentsAdjList()
###  getNewIntersection(undg,nodeSet,interNbrSet)
###  print "intersection nbr set: ",interNbrSet


### -----------------------
### Validation.
###  nodeSet = set()
###  nodeSet.add(1)
###  nodeV=4
###  existingInterNbrSet = set()
###  existingInterNbrSet.add(2)
###  existingInterNbrSet.add(3)
###  existingInterNbrSet.add(4)
###  newInterNbrSet = set()
###  getNewAdditionalIntersection(undg,nodeSet,existingInterNbrSet,nodeV,newInterNbrSet)
###  print "newInterNbrSet (should be empty): ",newInterNbrSet
###  
###  newInterNbrSet.clear()
###  nodeV=5
###  getNewAdditionalIntersection(undg,nodeSet,existingInterNbrSet,nodeV,newInterNbrSet)
###  print "newInterNbrSet (should be 2,3,4): ",newInterNbrSet


### -----------------------
### Validation.
### nodeV=2
### nbrSet = getGammaXplusv(undg,setX,gammaX,nodeV)
### print " neighbor set from  getGammaXplusv: ",nbrSet
### print " num neighbor set from  getGammaXplusv: ", getNumGammaXplusv(undg,setX,gammaX,nodeV)
### nodeV=5
### nbrSet = getGammaXplusv(undg,setX,gammaX,nodeV)
### print " neighbor set from  getGammaXplusv: ",nbrSet
### print " num neighbor set from  getGammaXplusv: ", getNumGammaXplusv(undg,setX,gammaX,nodeV)




### -----------------------
### Validation.
###  nodeSet.clear()
###  nodeSet.add(1)
###  nodeSet.add(2)
###  getNewIntersection(undg,nodeSet,interNbrSet)
###  print "intersection nbr set: ",interNbrSet
###  
###  nodeSet.clear()
###  nodeSet.add(1)
###  nodeSet.add(5)
###  getNewIntersection(undg,nodeSet,interNbrSet)
###  print "intersection nbr set: ",interNbrSet




fhout.close()

### Graph is done; now print.
endTime = time.time()
print(" elapsed time (s) : ",endTime-startTime)


print(" -- good termination --")

