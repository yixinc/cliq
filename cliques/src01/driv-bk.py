# -*- coding:utf-8 -*-

### cjk:  Test driver for GraphWbk class.
###    For verification.

import os
import sys
import time

### This temporarily alters your python path so that 
### we can test our new modules.  This enables us to 
### test and run without permanently putting in a 
### bunch of clutter into the PYTHONPATH.
### This is the path to the SkMap.py.
### This needs to be modified per user.
## sys.path.append('../')
sys.path.append('/home/ckuhlman/pscripts/')

import GraphWbk

infile =sys.argv[1]
outfile=sys.argv[2]
minCliqueSize = int(sys.argv[3])

startTime = time.time()


gbk = GraphWbk.graph()

gbk.readFromEdgeList(infile)

## This finds and prints to file all maximal cliques.
fhout = open(outfile,"w")
gbk.find_all_cliques(fhout,minCliqueSize)

endTime = time.time()

print (" total elapsed time (s), (hr): ",endTime-startTime,float(endTime-startTime)/3600.0)

print ("good termination")



