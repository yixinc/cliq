#  Finding Unique Cliques and Bicliques in Graphs

-------------------

### Purpose of this area.

This area contains codes to output unique cliques or bicliques, given
an input file that contains (computed) cliques or bicliques that may have
redundant values.

-------------------

## Directory unique-sets 

Contains codes for identifying unique cliques or bicliques in graphs.

That is, the same code can be used on a file of cliques or a file
of bicliques.
There is one test case for each type of output.
Note that a file has either all cliques in it, or all bicliques in it;
there is no mixing of cliques and bicliques ... but this code will not
care; it will run anyway.
We just do not want to mix cliques and bicliques.

The input for this code should be a file of cliques or bicliques 
that are ouputs from the codes under the directory _identify-cliques-bicliques-in-graphs/cliques_
or _identify-cliques-bicliques-in-graphs/bicliques_.

-------------------

### Subdirectories:   src01 and test01

**src01:**
This is the source code.  
A few notes:

1. The code has been updated just today to use python 3.
2. This is a serial code.



**test01:**
This is the test directory.
There are three test cases (that is, two different executions of the code).

To run tests:

1. go to the test01 directory.
2. create a conda python 3 virtual environment to run code in (this needs to be done only once).
    - see instructions below.
3. activate the conda virtual environment.
    - in example below, type:  conda activate py37-base
4. to run the test cases, type:  ./run.all
5. to test the output against valid output, type:  ./run.diff.all

Step 5 is a simple unix diff command, so no output means no differences between the
just-run output and the valid output.

Note there may be differences in the order in which the cliques appear in the output file.
I suppose this could be changed by altering the code to accept a seed for the random
number generator.

-------------------

### command line invocation

python ./path-to-src01/extractUniqueSets02.py  input-filename-of-cliques-or-bicliques  output-filename-containing-unique-cliques-or-bicliques  

Example:
python ../src01/extractUniqueSets02.py  try01.node.max.bicliques.dat  try01.unique.node.max.bicliques.dat


### Input file format
input-filename-of-cliques-or-bicliques:  file contains either all cliques or all bicliques.
Each line contains the nodes in one clique or biclique.
There can be any number of nodes on a line.
See the test cases for examples.


### Output file format
output-filename-containing-unique-cliques-or-bicliques:  has the set of unique cliques or bicliques. 
In the single output file, the nodes of one clique [or bicliques] are printed on one line of the
file.
So if there are q cliques in a graph, then there should be q lines in the output file.
The first value on each line is the number of the clique or biclique, starting
with "1."

-------------------

## Setting up environment to run python code.

### On laptop, using conda.

Create a conda virtual environment.  Any python version >= python 3.7 should be fine.
This is a very basic code.
I have tested code with python 3.7.

1.  At command line, outside of any virtual environment, type:  conda create -n myenv python=3.7 
    - where myenv is the name of the virtual environment (venv) that you specify.
    - assume that we use py37-base for the venv.
    - assume that we use python 3.7
    - so, this is:  conda create -n py37-base python=3.7

To activate the conda venv, do:

1. get out of any conda venv:  conda deactivate
2. activate this venv:  conda activate py37-base


