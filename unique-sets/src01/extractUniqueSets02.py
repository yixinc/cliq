# -*- coding:utf-8 -*-

### 01 oct 2013.
### cjk:  
### purpose:  Given a file where there is a set of integers on one line,
###           (there can be any number of *unique* integers), 
###           read in all of the lines (one frozenset per line),
###           and then do a comparison to determine whether
###           there are redundant sets of the same composition.
###           Save only one instance of each set.

###           Bottom line:  remove redundant sets from a collection of sets.

###           Output: unique sets.  Contents of one set per line.

import os
import sys

### This temporarily alters your python path so that 
### we can test our new modules.  This enables us to 
### test and run without permanently putting in a 
### bunch of clutter into the PYTHONPATH.
### This is the path to the SkMap.py.
### This needs to be modified per user.
### sys.path.append('/groups/NDSSL/projects/EMBERS/Soc/codes/dkmap/src')


### -----------------------------
### Start program.

### -----------------------------
### Start program.
if (len(sys.argv) != 3):
    print("  Error.  Incorrect usage.")
    print("  usage: exec infile outfile")
    print("  Halt.")
    quit()


### -------------------------
### Command line arguments.
infile = sys.argv[1]
outfile = sys.argv[2]

## Echo back.
print ("adj list file: ",infile)
print ("(output) uel file: ",outfile)


### -------------------------
### Read in edges and output immediately.
fhin = open(infile,"r")
fhout = open(outfile,"w")
countLines=0
outer = set()
inner = list()

### while (1):
for line in fhin:
    line02  = line.strip()
    if len(line02) == 0:
        continue 

    del inner[:]
    listEntries = line02.split()
    if len(listEntries) > 0:
        countLines += 1
        for item in listEntries:
            num = int(item)
            inner.append(num)

        ## Sort so that set can detect copies.
        inner.sort()
        innert = tuple(inner)
        outer.add(innert)

fhin.close()

print("number of sets stored: ",len(outer))
print("number of sets (lines) read in: ",countLines)
### print " elements: ",outer


### Sort tuples by size:  largest first.
xs = list(outer)
xs.sort(key = lambda s: len(s),reverse=True)


fhout = open(outfile,"w")

### Put counter in front.
count = 1
for tup in xs:
    fhout.write(str(count) + "      ")
    for item in tup:
        fhout.write(str(item) + " ")
    fhout.write("\n")
    count += 1

    
    
fhout.close()


print(" -- good termination --")

